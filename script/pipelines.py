# Define your item pipelines here
#
# Don't forget to add your pipeline to the ITEM_PIPELINES setting
# See: http://doc.scrapy.org/en/latest/topics/item-pipeline.html

from scrapy.contrib.pipeline.images import ImagesPipeline
from scrapy.exceptions import DropItem
from scrapy.http import Request

class AvitoPipeline(object):
    def process_item(self, item, spider):
        return item

class PhoneImagesPipeline(ImagesPipeline):

    def get_media_requests(self, item, info):
        if item['image_urls']:
            headers = {"Referer": item['url']}
            yield Request(item['image_urls'], headers=headers)

    def item_completed(self, results, item, info):
        image_paths = [x['path'] for ok, x in results if ok]
        if image_paths:
            item['phonefile'] = image_paths[0]
        return item
