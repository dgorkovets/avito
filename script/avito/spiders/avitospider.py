from avito.items import AvitoItem
from scrapy.spider import BaseSpider
from scrapy.selector import HtmlXPathSelector
from scrapy.http import Request
from scrapy import log, signals
from cities import CITIES
from phone_fetcher import decode_phone
import random
from avito.proxies import *
import base64

from scrapy.contrib.exporter import CsvItemExporter
class CSVkwItemExporter(CsvItemExporter):

    def __init__(self, *args, **kwargs):
        kwargs['fields_to_export'] = ['title', 'seller', 'description', 'phonefile', 'url']
        kwargs['encoding'] = 'utf-8'

        super(CSVkwItemExporter, self).__init__(*args, **kwargs)

class ProxyMiddleware(object):

    # overwrite process request
    def process_request(self, request, spider):
        proxy = random.choice(PROXIES)
        request.meta['proxy'] = "http://" + proxy
        #encoded_user_pass = base64.encodestring(PROXY_USERPASS)
        #request.headers['Proxy-Authorization'] = 'Basic ' + encoded_user_pass

class RandomUserAgent(object):
    """Selects a user agent at random from the list specifed by the USER_AGENTS
    setting to use for each request."""

    def __init__(self, user_agents):
        # Disable this middleware if user_agents is invalid, by raising
        # NotConfigured.
        try:
            random.choice(user_agents)
        except:
            raise "Error"

        self.user_agents = user_agents

    @classmethod
    def from_crawler(cls, crawler):
        settings = crawler.settings
        return cls(settings.get('USER_AGENTS'))

    def process_request(self, request, spider):
        user_agent = random.choice(self.user_agents)
        request.headers['User-Agent'] = user_agent

class AvitoSpider(BaseSpider):
    name = "avito"

    def __init__(self, category, *args, **kwargs):
        self.allowed_domains = ["avito.ru"]
        self.start_urls = ["http://www.avito.ru/%s/%s" % (city, category) for city in CITIES]
        super(AvitoSpider, self).__init__(*args, **kwargs)

    def parse(self, response):
        hxs = HtmlXPathSelector(response)

        item_links = hxs.select("//div[@class='b-catalog-table']//div[@class='item c-b-0']//div[@class='description']//a[@title]/@href").extract()
        for link in item_links:
            link = "http://avito.ru" + link
            yield Request(link, self.parse_item)

        next_page_link = hxs.select("//a[@class='next']/@href").extract()
        if next_page_link:
            
            link = "http://avito.ru" + next_page_link[0]
            yield Request(link, self.parse)


    def parse_item(self, response):
        hxs = HtmlXPathSelector(response)

        phonefile = ""
        phones = hxs.select("//div[@class='item']/script/text()").extract()
        phone_url = ""
        if phones:
            phones = phones[0]
            key = phones.split("'")[3]
            item_page_url = response.url.lstrip("http://www.avito.ru")
            item_id = int(item_page_url.split("_")[-1])
            item_page_url = item_page_url.replace("/","_")
            phone_url = "http://www.avito.ru/items/phone/%s?pkey=%s" % (item_page_url, decode_phone(key, item_id))
            

        item = AvitoItem()
        try:
            item["title"] = hxs.select("//h1[@class='item_title item_title-small']/text()").extract()[0]
        except:
            pass
        try:
            item["seller"] = hxs.select("//div[@id='seller']//strong/text()").extract()[0].strip()
            item["seller"] = hxs.select("//div[@id='seller']//strong/text()").extract()[0].strip()
        except:
            pass
        item["description"] = "".join(hxs.select("//div[@id='desc_text']//p/text()").extract()) 
        item["image_urls"] = phone_url
        item["url"] = response.url
        return item

