# Define here the models for your scraped items
#
# See documentation in:
# http://doc.scrapy.org/en/latest/topics/items.html

from scrapy.item import Item, Field

class AvitoItem(Item):
    title = Field()
    seller = Field()
    description = Field()
    phonefile = Field()
    url = Field()
    image_urls=Field()
    phoneurl=Field()
