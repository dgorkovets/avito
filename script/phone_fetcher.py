import re

def decode_phone(key, item_id):
    prog = re.compile(r"[0-9a-f]+")
    chunks = prog.findall(key)
    if item_id % 2 == 0:
        chunks = reversed(chunks)
    mixed = ''.join(chunks)
    return mixed[0:-1:3] 

if __name__ == "__main__":
    print decode_phone("f8a387855f4dm7329bb60bmed8mce9a1ec88m2aem051mbf454fda80d3m2d46550789a8db0ebb92348d1889e149efea81533d08a",261019178)
