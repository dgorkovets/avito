#!/bin/bash


OUT_DIR=/media/WIN7/avito_results
#OUT_DIR=`pwd`
#CATEGORIES=(odezhda_obuv_aksessuary detskaya_odezhda_i_obuv tovary_dlya_detey_i_igrushki chasy_i_ukrasheniya krasota_i_zdorove)
CATEGORIES=(tovary_dlya_detey_i_igrushki chasy_i_ukrasheniya krasota_i_zdorove)

for CATEGORY in ${CATEGORIES[*]}
do

    DOMAIN=avito.ru
    FEED_DIR=$OUT_DIR/$DOMAIN\_result/$CATEGORY
    FEED_URI=$FEED_DIR\/out.csv
    IMAGES_STORE=$FEED_DIR\/img

    scrapy crawl avito -a category=$CATEGORY  --set IMAGES_STORE=$IMAGES_STORE --set FEED_URI=$FEED_URI

done


